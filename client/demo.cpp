#include <iostream>
#include <fstream>
#include <vector>

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

using namespace std;

struct sample
{
	int camera_id;
	
    short int x;
	short int y;
	
    short int scale;
	float response;    
};

// Generate a random message 
vector<sample> generateDetections()
{
	vector <sample> x;

	sample object1;
	
	object1.camera_id = 1;
	
	object1.x = random() % 640;
	object1.y = random() % 480;
	object1.scale = 5;
	
	object1.response = 10 + random() % 5;
	
	x.push_back(object1);
	
	return x;	
}





main() 
{
		while(1) 
		{
			usleep(300000);
			
			vector<sample>  x = generateDetections();
						
			printf("MQTT MESSAGE TO SEND: camera:%d location:%d,%d response:%f\n", x[0].camera_id, x[0].x, x[0].y, x[0].response);	
		}
}





