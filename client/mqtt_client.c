//
// Created by Wester on 16-5-28.
//

//using namespace std;

//#include <iostream>
//#include <fstream>
//#include <vector>

#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>

#include <mosquitto.h>
#include "client_shared.h"
#include <libxml2/libxml/parser.h>

#define STATUS_CONNECTING 0
#define STATUS_CONNACK_RECVD 1
#define STATUS_WAITING 2

/* Global variables for use in callbacks. See sub_client.c for an example of
 * using a struct to hold variables for use in callbacks. */
static char *topic = NULL;
static char *message = NULL;
static long msglen = 0;
static int qos = 0;
static int retain = 0;
static int mode = MSGMODE_NONE;
static int status = STATUS_CONNECTING;
static int mid_sent = 0;
static int last_mid = -1;
static int last_mid_sent = -1;
static bool connected = true;
static char *username = NULL;
static char *password = NULL;
static bool disconnect_sent = false;
static bool quiet = false;

struct sample
{
	int camera_id;

	short int x;
	short int y;

	short int scale;
	float response;
};


// Generate a random message
struct sample generateDetections()
{
	struct sample object1;

	object1.camera_id = 1;

	object1.x = (short) (random() % 640);
	object1.y = (short) (random() % 480);
	object1.scale = 5;

	object1.response = 10 + random() % 5;

	return object1;
}


void my_connect_callback(struct mosquitto *mosq, void *obj, int result)
{
	int rc = MOSQ_ERR_SUCCESS;

	if(!result){
		switch(mode){
			case MSGMODE_CMD:
			case MSGMODE_FILE:
			case MSGMODE_STDIN_FILE:
				rc = mosquitto_publish(mosq, &mid_sent, topic, (int) msglen, message, qos, (bool) retain);
				break;
			case MSGMODE_NULL:
				rc = mosquitto_publish(mosq, &mid_sent, topic, 0, NULL, qos, (bool) retain);
				break;
			case MSGMODE_STDIN_LINE:
				status = STATUS_CONNACK_RECVD;
				break;
			default:break;
		}
		if(rc){
			if(!quiet){
				switch(rc){
					case MOSQ_ERR_INVAL:
						fprintf(stderr, "Error: Invalid input. Does your topic contain '+' or '#'?\n");
						break;
					case MOSQ_ERR_NOMEM:
						fprintf(stderr, "Error: Out of memory when trying to publish message.\n");
						break;
					case MOSQ_ERR_NO_CONN:
						fprintf(stderr, "Error: Client not connected when trying to publish.\n");
						break;
					case MOSQ_ERR_PROTOCOL:
						fprintf(stderr, "Error: Protocol error when communicating with broker.\n");
						break;
					case MOSQ_ERR_PAYLOAD_SIZE:
						fprintf(stderr, "Error: Message payload is too large.\n");
						break;
					default:break;
				}
			}
			mosquitto_disconnect(mosq);
		}
	}else{
		if(!quiet){
			fprintf(stderr, "%s\n", mosquitto_connack_string(result));
		}
	}
}

void my_disconnect_callback(struct mosquitto *mosq, void *obj, int rc)
{
	connected = false;
}

void my_publish_callback(struct mosquitto *mosq, void *obj, int mid)
{
	last_mid_sent = mid;
	if(mode == MSGMODE_STDIN_LINE){
		if(mid == last_mid){
			mosquitto_disconnect(mosq);
			disconnect_sent = true;
		}
	}else if(!disconnect_sent){
		mosquitto_disconnect(mosq);
		disconnect_sent = true;
	}
}

void my_log_callback(struct mosquitto *mosq, void *obj, int level, const char *str)
{
	printf("%s\n", str);
}


char* get_param(char* tag_name)
{
	xmlDoc         *document;
	xmlNode        *root, *first_child, *node;
	char           *filename;

	filename = (char *) "config.xml";

	document = xmlReadFile(filename, NULL, 0);
	root = xmlDocGetRootElement(document);

	first_child = root->children;
	for (node = first_child; node; node = node->next) {
		if (strcmp((char *)node->name, tag_name) == 0){
			return (char *) xmlNodeGetContent(node);
		}
	}
	return NULL;
}

int send_msg(char *msg, int argc, char *argv[]){
	struct mosq_config cfg;
	struct mosquitto *mosq = NULL;
	int rc;
	char *buf;
	int buf_len = 1024;


	status = STATUS_CONNECTING;
	mid_sent = 0;
	last_mid = -1;
	last_mid_sent = -1;
	connected = true;
	disconnect_sent = false;


	buf = (char*)malloc((size_t) buf_len);
	if(!buf){
		fprintf(stderr, "Error: Out of memory.\n");
		return 1;
	}

	memset(&cfg, 0, sizeof(struct mosq_config));
	rc = client_config_load(&cfg, CLIENT_PUB, argc, argv);
	if(rc){
		client_config_cleanup(&cfg);
		return 1;
	}

//	 update params from xml file
	char *new_host = get_param((char *) "HOST");
	if ( new_host){
		cfg.host = new_host;
	}

	char *new_topic = get_param((char *) "TOPIC");
	if (new_topic){
		cfg.topic = get_param((char *) "TOPIC");
	}

	cfg.message = strdup(msg);
	cfg.msglen = strlen(cfg.message);
	cfg.pub_mode = 1;
	topic = cfg.topic;
	message = cfg.message;
	msglen = cfg.msglen;
	qos = cfg.qos;
	retain = cfg.retain;
	mode = cfg.pub_mode;
	username = cfg.username;
	password = cfg.password;
	quiet = cfg.quiet;

	mosquitto_lib_init();

	if (client_id_generate(&cfg, "mosqpub")) {
		return 1;
	}

	mosq = mosquitto_new(cfg.id, true, NULL);
	if (!mosq) {
		switch (errno) {
			case ENOMEM:
				if (!quiet) fprintf(stderr, "Error: Out of memory.\n");
				break;
			case EINVAL:
				if (!quiet) fprintf(stderr, "Error: Invalid id.\n");
				break;
			default:
				break;
		}
		mosquitto_lib_cleanup();
		return 1;
	}

	if (cfg.debug) {
		mosquitto_log_callback_set(mosq, my_log_callback);
	}

	mosquitto_connect_callback_set(mosq, my_connect_callback);
	mosquitto_disconnect_callback_set(mosq, my_disconnect_callback);
	mosquitto_publish_callback_set(mosq, my_publish_callback);

	if (client_opts_set(mosq, &cfg)) {
		return 1;
	}

	rc = client_connect(mosq, &cfg);

	if (rc) return rc;

	do {
		rc = mosquitto_loop(mosq, -1, 1);
	} while (rc == MOSQ_ERR_SUCCESS && connected);


	if (rc) {
		fprintf(stderr, "Error: %s\n", mosquitto_strerror(rc));
		return rc;
	}

	mosquitto_destroy(mosq);
	mosquitto_lib_cleanup();
//	fprintf(stderr, "Message is sent, %s", msg);
	return rc;
}

int main(int argc, char *argv[])
{

	int i =0;
	for (i = 0; i < 10; i ++){

		struct sample x = generateDetections();

		char tmp_buf[512];
		sprintf(tmp_buf, "MQTT MESSAGE TO SEND(%d): camera:%d location:%d,%d response:%f\n", i, x.camera_id,
				x.x, x.y, x.response);

		printf(tmp_buf);

		int ret = send_msg(tmp_buf, argc, argv);

//		fprintf(stderr, "Result(%d): %d\n", i, ret);

		sleep(1);
	}


}
